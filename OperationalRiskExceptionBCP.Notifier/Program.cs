﻿using NLog;
using OperationalRiskException.EscalationTopShelf.ExcalationService;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace OperationalRiskException.EscalationTopShelf
{
    class Program
    {
        static void Main(string[] args)
        {


            var rc = HostFactory.Run(x =>
            {
                x.Service<ExcalationSchedule>(s =>
                {
                    s.ConstructUsing(name => new ExcalationSchedule());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();
                string path = AppDomain.CurrentDomain.BaseDirectory; //Path.GetTempPath();

                //var config = new NLog.Config.LoggingConfiguration();

                // Targets where to log to: File and Console
                //var logfile = new NLog.Targets.FileTarget("logfile") { FileName = $"{path}/logs/app.log" };
                //var logconsole = new NLog.Targets.ConsoleTarget("logconsole");
                //Console.WriteLine("Path:: " + $"{path}/logs/app.log");
                //// Rules for mapping loggers to targets            
                //config.AddRule(LogLevel.Info, LogLevel.Fatal, logconsole);
                //config.AddRule(LogLevel.Debug, LogLevel.Fatal, logfile);

                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Information()
                    .WriteTo.Console()
                    .WriteTo.File($"{path}logs\\app.log", rollingInterval: RollingInterval.Day)
                    .CreateLogger();
                x.UseSerilog();



                x.SetDescription("Engine to handle processing of Excalation");
                x.SetDisplayName("OperationalRiskExceptionKRI.Notifier");
                x.SetServiceName("OperationalRiskExceptionKRI.Notifier");
            });



            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }

}
