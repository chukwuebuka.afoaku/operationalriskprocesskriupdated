﻿
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.EscalationTopShelf.ExcalationService
{
    public class SchedulingImplementation : IJob
    {
        private IEscalationService _escalationService;
        public SchedulingImplementation()
        {
            _escalationService = new EscalationService(); ;

        }

        public async Task Execute(IJobExecutionContext context)
        {
         await   _escalationService.FirstEscalationImplementation();
           // await Task.FromResult<bool>(true);

        }
    }

    public class OtherSchedulingImplementation : IJob
    {
        private IEscalationService _escalationService;
        public OtherSchedulingImplementation()
        {
            _escalationService = new EscalationService(); ;

        }

        public async Task Execute(IJobExecutionContext context)
        {
        await _escalationService.ContinuousEscalationImplementation();

        }
    }

    public class SoftWarningSchedulingImplementation : IJob
    {
        private IEscalationService _escalationService;
        public SoftWarningSchedulingImplementation()
        {
            _escalationService = new EscalationService(); ;

        }

        public async Task Execute(IJobExecutionContext context)
        {
            await _escalationService.SoftWarningEscalation();

        }
    }

    public class ChampionAssessmentNotification : IJob
    {
        private IAssessmentNotificationService _assessmentNotificationService;
        public ChampionAssessmentNotification()
        {
            _assessmentNotificationService = new AssessmentNotificationService(); ;

        }

        public async Task Execute(IJobExecutionContext context)
        {
            var template = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["assessmentTemplate"] as string;

            var count= _assessmentNotificationService.EmailSendingPeriodOfAssessment(template);
            await Task.FromResult(false);


        }
    }
}

