﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.EscalationTopShelf.ExcalationService
{
 public   class ExcalationSchedule
    {
        private readonly IScheduler scheduler;
        //private string ReconTimeOfDay = ConfigurationManager.AppSettings.Get("ReconTimeOfDay");
        private string ReconUpdateTimeOfDay = ConfigurationManager.AppSettings.Get("ReconUpdateTimeOfDay");
      //  private string ReconTimeOfDay = "0 0/2 * 1/1 * ? *";
     //   private string ReconUpdateTimeOfDay = "0 0/2 * 1/1 * ? *";
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public ExcalationSchedule()
        {
            NameValueCollection props = new NameValueCollection
        {
            { "quartz.serializer.type", "binary" },
            { "quartz.scheduler.instanceName", "OperationalRiskException.EscalationTopShelf" },
            { "quartz.jobStore.type", "Quartz.Simpl.RAMJobStore, Quartz" },
            { "quartz.threadPool.threadCount", "4" }
        };
            StdSchedulerFactory factory = new StdSchedulerFactory(props);
            scheduler = factory.GetScheduler().ConfigureAwait(false).GetAwaiter().GetResult();
        }
        public void Start()
        {
            scheduler.Start().ConfigureAwait(false).GetAwaiter().GetResult();
            ScheduleJobs();
        }
        public void ScheduleJobs()
        {
            //first Escalation
            IJobDetail job = JobBuilder.Create<SchedulingImplementation>()
                  .WithIdentity("myJob", "group")
                  .Build();


            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger", "group")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(23)
                    .RepeatForever())
                .ForJob("myJob", "group")
                .Build();



            //Update Cron Job
            IJobDetail job1 = JobBuilder.Create<OtherSchedulingImplementation>()
                      .WithIdentity("myJob1", "group1")
                      .Build();
            ITrigger trigger1 = TriggerBuilder.Create()
                .WithIdentity("trigger1", "group1")
               .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                    .RepeatForever())
                .ForJob("myJob1", "group1")
                .Build();  
            IJobDetail job2 = JobBuilder.Create<SoftWarningSchedulingImplementation>()
                      .WithIdentity("myJob2", "group2")
                      .Build();
            ITrigger trigger2 = TriggerBuilder.Create()
                .WithIdentity("trigger2", "group2")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(24)
                    .RepeatForever())
                // .WithCronSchedule(ReconUpdateTimeOfDay)
                .ForJob("myJob2", "group2")
                .Build(); 


            IJobDetail job3 = JobBuilder.Create<ChampionAssessmentNotification>()
                      .WithIdentity("myJob3", "group3")
                      .Build();
            _logger.Info("Testing");
            ITrigger trigger3 = TriggerBuilder.Create()
                .WithIdentity("trigger3", "group3")
                 .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInHours(22)
                    .RepeatForever())
                .ForJob("myJob3", "group3")
                .Build();

             scheduler.ScheduleJob(job, trigger).ConfigureAwait(false).GetAwaiter().GetResult();
            scheduler.ScheduleJob(job1, trigger1).ConfigureAwait(false).GetAwaiter().GetResult();
            scheduler.ScheduleJob(job2, trigger2).ConfigureAwait(false).GetAwaiter().GetResult();
            scheduler.ScheduleJob(job3, trigger3).ConfigureAwait(false).GetAwaiter().GetResult();
        }
        public void Stop()
        {
            scheduler.Shutdown().ConfigureAwait(false).GetAwaiter().GetResult();
        }
    }
}

