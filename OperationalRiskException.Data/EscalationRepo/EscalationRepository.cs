﻿using Dapper;
using NLog;
using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.Dtos;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.EscalationRepo
{
   public  class EscalationRepository: IEscalationRepository { 
    
        readonly IDbConnection _sqldb;
        readonly IDbConnection _sqldbApproval;
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

    private IRepository<EscalationProgress> _escalationProgressRepository;
    private IRepository<Escalation> _escalationRepository;
    //dbo.SelectUsersNotYetTakenAssessment
    public EscalationRepository()
    {
        _sqldb = ConnectionFactory.GetSqlConnection();
        _sqldbApproval = ConnectionFactory.GetSqlConnectionApproval();
        _escalationProgressRepository = new Repository<EscalationProgress>();
        _escalationRepository = new Repository<Escalation>();
    }


    public List<ChampionEscalationDTO> WarningEscalation()
    {
        var sqlProc = "[spUsersWarningList]";
        //  var daysConfigurationInterval = Convert.ToInt32(ConfigurationManager.AppSettings["StartDayInternal"].ToString());
        // var todaysDate = DateTime.Now;
        var TodayDATE = DateTime.Now.ToString("yyyy-MM-dd");
        //var TodayDATE = DateTime.Now.ToString("yyyy-MM-dd");
        var result = _sqldb.Query<ChampionEscalationDTO>(sqlProc, new { TodayDATE },
            commandType: CommandType.StoredProcedure);
        return result.ToList();

    }


    public List<ChampionEscalationDTO> PendingEscalation()
    {
        var sqlProc = "SelectUsersNotYetTakenAssessment";
        var daysConfigurationInterval = Convert.ToInt32(ConfigurationManager.AppSettings["StartDayInternal"].ToString());
        // var todaysDate = DateTime.Now;
        var TodayDATE = DateTime.Now.AddDays(-daysConfigurationInterval).ToString("yyyy-MM-dd");

        //var TodayDATE = DateTime.Now.ToString("yyyy-MM-dd");
        var result = _sqldb.Query<ChampionEscalationDTO>(sqlProc, new { TodayDATE },
            commandType: CommandType.StoredProcedure);
        return result.ToList();

    }


    public CheckUp InsertFirstEscalationRecords(List<ChampionEscalationDTO> championEscalationLists )
    {
            var listcheck = new CheckUp();
            try
        {
                //FirstEscalation Save
              
                if (championEscalationLists.Count > 0)
            {
                logger.Info("championEscalationLists.Count::::" + championEscalationLists.Count.ToString());
                    var listOfChamps = new List<ChampionEscalationDTO>();
                var parameters = new List<DynamicParameters>();
                foreach (var item in championEscalationLists)
                {
                    var p = new DynamicParameters();
                    p.Add("@ModifiedDate", DateTime.Now.ToString());
                    p.Add("@CreatedDate", DateTime.Now.ToString());
                    p.Add("@CreatedBy", "BackgroundService");
                    p.Add("@IsDeleted", false);
                    p.Add("@ModifiedBy", "BackgroundService");
                    p.Add("@LevelOfEscalation", 1);
                    p.Add("@AssessmentSchedulerId", item.AssessmentSchedulerId);
                    p.Add("@UserId", item.UserId);

                    p.Add("@@ChampionEmail", item.Username);
                    p.Add("@HasEscalationStopped", false);
                        var checkIfTheRecordExistBefore = _escalationRepository.GetAllIncluding(x => x.UserId == item.UserId && x.AssessmentSchedulerId == item.AssessmentSchedulerId, false).FirstOrDefault();

                    if(checkIfTheRecordExistBefore==null)
                        {
                            parameters.Add(p);
                            listOfChamps.Add(item);
                        }
                        
                    
                }
                //Pending  
                var sqlQueryUpdate = "sp_InsertEscalationQuery";
                int rowsAffected = _sqldb.Execute(sqlQueryUpdate, parameters, commandType: CommandType.StoredProcedure);
                logger.Info("Number of row saved::::" + rowsAffected.ToString());
                    listcheck.ChampionEscalations = listOfChamps;
                    listcheck.Count = rowsAffected;

                return listcheck;


            }
            else
            {
                    return listcheck;
                }
        }
        catch (Exception exx)
        {
            logger.Error(exx.ToString());
                return listcheck;
            }
    }




    public bool SaveFirstEscalation(string supervUser, string SupervisorRole, ChampionEscalationDTO escalationDTO)
    {
        //new Model
        try
        {
            var query = _escalationRepository.GetSingleRecordFilter(x => x.UserId == escalationDTO.UserId
            && x.AssessmentSchedulerId == escalationDTO.AssessmentSchedulerId && !x.IsDeleted);

            logger.Info("Check Value of _escalationId" + query.Id.ToString());
            var insertEnt = new EscalationProgress()
            {
                CreatedDate = DateTime.Now,
                EscalationId = query.Id,
                LevelOfEscalation = query.LevelOfEscalation,
                SupervisorEmail = supervUser,
                IsActive = true,
                SupervisorRole = SupervisorRole
            };
            _escalationProgressRepository.Insert(insertEnt);
            var check = _escalationProgressRepository.Save();
            return check > 0;
        }
        catch (Exception exx)
        {
            logger.Error("SaveFirstEscalation::::", exx.ToString());
            return false;
        }
    }

    public bool SaveOtherEscalation(string supervUser, string SupervisorRole, ONEscalationChampionDTO pendingItem)
    {


        try
        {
            var query = _escalationProgressRepository.GetAllIncluding(x => x.EscalationId == pendingItem.EscalationId && x.IsActive, true).FirstOrDefault();
            if (query != null)
            {
                query.IsActive = false;
                _escalationProgressRepository.Update(query);
                var checkUpdate = _escalationProgressRepository.Save() > 0;
                logger.Info("checkUpdate::::" + checkUpdate.ToString());

                var escalationEntity = _escalationRepository.GetAllIncluding(x => x.Id == pendingItem.EscalationId, true).FirstOrDefault();
                escalationEntity.LevelOfEscalation = escalationEntity.LevelOfEscalation + 1;
                _escalationRepository.Update(escalationEntity);
                var cUpdateescalationEntity = _escalationRepository.Save() > 0;
                logger.Info("cUpdateescalationEntity:::: " + cUpdateescalationEntity.ToString());

            }
            else
            {
                logger.Info("No Record to be Updated ::::");
                return false;
            }
            logger.Info("Check Value of _escalationId" + query.Id.ToString());
            var insertEnt = new EscalationProgress()
            {
                CreatedDate = DateTime.Now,
                EscalationId = query.Id,
                LevelOfEscalation = query.LevelOfEscalation + 1,
                SupervisorEmail = supervUser,
                IsActive = true,
                SupervisorRole = SupervisorRole

            };
            _escalationProgressRepository.Insert(insertEnt);
            var check = _escalationProgressRepository.Save();
            return check > 0;
        }
        catch (Exception exx)
        {
            logger.Error("ErrorEscalation::::", exx.ToString());
            return false;
        }

    }
    public HRStaffObject GetSTAFF_USER(string username)
    {
        var usern = username.Replace("@ubagroup.com", "");
        var sqlQuery = "[sp_StaffByUserName]";
        var query = _sqldbApproval.QueryFirstOrDefault<HRStaffObject>(sqlQuery, new { username = usern },
            commandType: CommandType.StoredProcedure);
        return query;
    }

    public List<ONEscalationChampionDTO> OnEscalationList()
    {
        //Pending web.config ConfigurationInterval
        var daysConfigurationInterval = Convert.ToInt32(ConfigurationManager.AppSettings["ConfigurationInterval"].ToString());
        // var todaysDate = DateTime.Now;
        var datePeriod = DateTime.Now.AddDays(-daysConfigurationInterval).ToString("yyyy-MM-dd");
        //pending
        var sqlQuery = "[ChampionOnEscalation]";
        var query = _sqldb.Query<ONEscalationChampionDTO>(sqlQuery,
            new { DateRange = datePeriod },
            commandType: CommandType.StoredProcedure);
        return query.ToList();

    }



}
}
