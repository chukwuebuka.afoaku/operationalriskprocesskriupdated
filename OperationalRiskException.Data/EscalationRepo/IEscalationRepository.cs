﻿using OperationalRiskException.Core.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Data.EscalationRepo
{
    public interface IEscalationRepository
    {
        Core.AuthVM.HRStaffObject GetSTAFF_USER(string username);
        CheckUp InsertFirstEscalationRecords(List<ChampionEscalationDTO> championEscalationLists);
        List<ONEscalationChampionDTO> OnEscalationList();
        List<ChampionEscalationDTO> PendingEscalation();
        bool SaveFirstEscalation(string supervUser, string SupervisorRole, ChampionEscalationDTO escalationDTO);
        bool SaveOtherEscalation(string supervUser, string SupervisorRole, ONEscalationChampionDTO pendingItem);
        List<ChampionEscalationDTO> WarningEscalation();
    }
}
