﻿namespace OperationalRiskException.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Wowo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SaveAssessmentByCategories", "AssessmentSetupId", c => c.Int());
            CreateIndex("dbo.SaveAssessmentByCategories", "AssessmentSetupId");
            AddForeignKey("dbo.SaveAssessmentByCategories", "AssessmentSetupId", "dbo.AssessmentSetups", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SaveAssessmentByCategories", "AssessmentSetupId", "dbo.AssessmentSetups");
            DropIndex("dbo.SaveAssessmentByCategories", new[] { "AssessmentSetupId" });
            DropColumn("dbo.SaveAssessmentByCategories", "AssessmentSetupId");
        }
    }
}
