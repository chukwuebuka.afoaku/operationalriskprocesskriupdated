﻿using OperationalRiskException.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Entities
{
    public class Question : BaseEntity
    {
        public int Id { get; set; }
        public string QuestionText { get; set; }
        public string BatchNo { get; set; }
        public QuestionCategory QuestionCategory { get; set; }
        public int? QuestionCategoryId { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public User QuestionCreatedOrModifiedBy { get; set; }
        public int QuestionCreatedOrModifiedById { get; set; }
        public User ApprovedBy { get; set; }
        public int? ApprovedById { get; set; }
        public QuestionTypeEnum QuestionType { get; set; }
        public string AnswerTextA { get; set; }
        public string AnswerTextB { get; set; }
        public string AnswerTextC { get; set; }
        public string AnswerTextD { get; set; }
        public string AnswerTextE { get; set; }
        public string RightAnswer { get; set; }
        public ChoiceAnswer ChoiceAnswer { get; set; }
        public double NumericAnswer { get; set; }
        


    }
    public class Approval : BaseEntity
    {
        public long Id { get; set; }
        public Question Question { get; set; }
        public int QuestionId { get; set; }
        public string QuestionHistory { get; set; }
        public int? QuestionCreatedById { get; set; }
        public User QuestionCreatedBy { get; set; }
        public int? ApprovedOrRejectById { get; set; }
        public User ApprovedOrRejectBy { get; set; }
        public ApprovalStatus ApprovalStatus { get; set; }
        public bool IsActive { get; set; }
    }
    public enum ApprovalStatus
    {
        Pending=0,
        Approved,
        Rejected

    }

}
