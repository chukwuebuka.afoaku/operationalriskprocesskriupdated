﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
   public class ReportModel
    {

        public string DepartmentName { get; set; }
        public string Username { get; set; }
        public int CategoryId { get; set; }
        public int CategoryScore { get; set; }
        public int TotalQuestion { get; set; }
        public string CategoryName { get; set; }
        public double PercentageScore { get; set; }
        public string AssessmentName { get; set; }

    }

    public class HeaderView
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
    public class FullReport
    {
        public  List<HeaderView> Headers { get; set; }
        public List<ReportModel> Reports { get; set; }

    }
}
