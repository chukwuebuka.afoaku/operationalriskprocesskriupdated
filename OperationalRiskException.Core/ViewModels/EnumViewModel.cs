﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
   public class EnumViewModel
    {
        public string Name { get; set; }
        public int Value { get; set; }
        public string Description { get; set; }
    }
}
