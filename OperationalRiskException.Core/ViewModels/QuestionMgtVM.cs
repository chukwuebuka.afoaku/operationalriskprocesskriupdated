﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.ViewModels
{
    public class QuestionMgtVM
    {
        public int QuestionCategoryId { get; set; }
        public string CategoryName { get; set; }
        public int UserId { get; set; }
        public List<TestQuestionVM> TestQuestion { get; set; }
        public int Count { get; set; }
    }
}
