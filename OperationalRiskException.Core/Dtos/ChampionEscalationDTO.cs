﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Core.Dtos
{
    public class ChampionEscalationDTO
    {
        //  US.Username, US.DepartmentName, ASET.AssessmentName,ASET.StartDate, ASET.EndDate, US.Id AS UserId, 
        //ASCH.Id AS AssessmentSchedulerId
        public string Username { get; set; }
        public string DepartmentName { get; set; }
        public string AssessmentName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int UserId { get; set; }
        public int AssessmentSchedulerId { get; set; }


    }

    public class CheckUp
    {
     public List<ChampionEscalationDTO> ChampionEscalations { get; set; }
        public int Count { get; set; }
    }
    public class ONEscalationChampionDTO
    {
        //EscalationId,US.Username,US.DepartmentName, EP.SupervisorEmail, EP.SupervisorRole
        public int EscalationId { get; set; }
        public string Username { get; set; }
        public string DepartmentName { get; set; }
        public string SupervisorEmail { get; set; }
        public string SupervisorRole { get; set; }



    }
}
