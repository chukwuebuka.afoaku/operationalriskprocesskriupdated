﻿using OperationalRiskException.Core.Entities;
using OperationalRiskException.Services.Services;
using OperationalRiskException.Services.Utilities;
using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace OperationalRiskException.AuditLogFilter
{
    public class LogActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //var req = filterContext.HttpContext.Request;
            //var username = req.Params["Username"];
            Log("OnActionExecuting", filterContext.RouteData, filterContext.HttpContext.Request);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Log("OnActionExecuted", filterContext.RouteData, filterContext.HttpContext.Request);
        }

       


        private void Log(string methodName, RouteData routeData, HttpRequestBase req)
        {
            try
            {
                var controllerName = routeData.Values["controller"];
                var actionName = routeData.Values["action"];
                var message = string.Format("{0} controller:{1} action:{2}", methodName, controllerName, actionName);
               
                var v = new AuditTrail
                {
                    moduleName = methodName,
                    ActionType = actionName.ToString(),
                    Comments = message,
                    PerformedBy = Utility.GetRoleViewModel().Username ?? req.Params["Username"],
                    PerformedOn = Utility.GetCurrentLocalDateTime().ToString("dd/MM/yyyy hh:mm:ss tt"),
                    Role = Utility.GetRoleViewModel().Rolename ?? "Anonymous",
                    ComputerName = req?.UserAgent,
                    Ipaddress = req?.UserHostAddress == "::1" ? "127.0.0.1" : req?.UserHostAddress,
                    Id = 0
                };
                var _userSrv = new AuditTrailServices();
                _userSrv.AddAudit(v);
            }
            catch //(Exception)
            {
                //  throw;
            }
        }

        private string GetClientIpAddress(HttpRequestMessage request)
        {
            try
            {
                if (request.Properties.ContainsKey("MS_HttpContext"))
                {
                    return IPAddress.Parse(((HttpContextBase)request.Properties["MS_HttpContext"]).Request.UserHostAddress).ToString();
                }
                //if (request.Properties.ContainsKey("MS_OwinContext"))
                //{
                //    return IPAddress.Parse(((OwinContext)request.Properties["MS_OwinContext"]).Request.RemoteIpAddress).ToString();
                //}
            }
            catch //(Exception)
            {
                return string.Empty;
            }
            return string.Empty;
        }
    }
}