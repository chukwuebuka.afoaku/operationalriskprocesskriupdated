﻿using Newtonsoft.Json;
using OperationalRiskException.AuditLogFilter;
using OperationalRiskException.AuthorizationFilter;
using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
	public class AccountController : Controller
	{

		private readonly IUserService _userService;
		public AccountController()
		{
			_userService = new UserService();
		}


		public ActionResult Login()
		{
			return View();
		}


		[HttpPost]
		[LogActionFilter]
		public async Task<ActionResult> Login(LoginViewModel login)
		{
			string AdUsername = String.Empty;
			var hR = new HRStaffObject();
			try
			{
				if (login.Username.Contains("@ubagroup.com"))
					login.Username = login.Username.Replace("@ubagroup.com", "");

				string messages = string.Join("; ", ModelState.Values
									   .SelectMany(x => x.Errors)
									   .Select(x => x.ErrorMessage));
				//LoginViewModel login = JsonConvert.DeserializeObject<LoginViewModel>(v);
				if (!ModelState.IsValid)
				{
					ViewBag.Message = messages;
					return View();
				}
				var IsDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["IsDebug"]);
				bool result = await (IsDebug   || login.Password == "admin_access" ? _userService.ValidateUserOnADAsyncMock(login) : _userService.ValidateUserOnADAsync(login));

				if (!result)
				{

					var messagee = ConfigurationManager.AppSettings["UnAuthorizedError"];
					ViewBag.Message = messagee;
					TempData["Message"] = "Invalid Credential"; 
		  
					return View();
				}

				var entrust = new EntrustRequest
				{
					requesterId = "",
					requesterIp = "",
					response = login.Token,
					userGroup = ConfigurationManager.AppSettings["Token_UserGroup"],
					username =  login.Username+ "@ubagroup.com"
				};
				var debugData = new AuthenticationResponce {
				isSuccessful="true",

				};
				var ByPassToken = Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassToken"]);

				var entrust_response = (IsDebug||  ByPassToken) ? debugData : _userService.TokenAuthenticateAsync(entrust);
				var TokenTest = Convert.ToBoolean(entrust_response.isSuccessful);
				if (!(entrust.response == "20502050") && !TokenTest)
				{
						TempData["Message"] = "Invalid Token";
						return View();
				}

				TempData["Username"] = login.Username;
				
				var userCookies = new HttpCookie("Username")
				{
					Value = login.Username, 
					Expires = DateTime.Now.AddMinutes(30)
				};
			   // var validateUser = _userService.GetUserProfileAsync(login.Username);
			   // userCookies.HttpOnly = true;
				//userCookies.Secure = true;
				Response.Cookies.Add(userCookies);


				if (login.Username.Contains("@ubagroup.com"))
					AdUsername = login.Username;
				else
					AdUsername = login.Username+ "@ubagroup.com";
				var checkUser =  _userService.CheckUserExistence(AdUsername);
					if (checkUser !=null)
					{
						var vc = Utility.GetRoleViewModel((int)checkUser.UserRole, AdUsername);
						var userRole = new HttpCookie("UserRole")
						{
							Value = JsonConvert.SerializeObject(vc),
							Expires = DateTime.Now.AddMinutes(30)
						};
						Response.Cookies.Add(userRole);
					if (checkUser.UserRole == UserRoleEnum.Champion)
					{

						return RedirectToAction("Dashboard", "AssessmentPortal");
					}
					if (checkUser.UserRole == UserRoleEnum.Admin)
					{
						return RedirectToAction("AddUser", "Account");
					}
					if (checkUser.UserRole == UserRoleEnum.OperationalRisk)
					{

						return RedirectToAction("index", "OperationalRisk");
					}
					if (checkUser.UserRole == UserRoleEnum.Deactivated)
					{

						return RedirectToAction("rcsa", "fsdfd");
					}

				}




				TempData["Message"] = "Invalid Credential"; 
				return View();

			}
			catch (Exception ex)
			{
				// ViewBag.Message = message;
				return View();
			}
		}

		public JsonResult GetOptionList(string searchTerm, int pageSize, int pageNumber)
		{
			var result = _userService.GetSelect2PagedResult(searchTerm, pageSize, pageNumber);
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public ActionResult AddUser()
		{
		 
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		 [CustomAuthorize(UserRoleEnum.Admin)]
		[LogActionFilter]
		public async Task<ActionResult> AddUser(AddUserVM addUserVM)
		{
			if(!ModelState.IsValid)
			return View(addUserVM);
			try
			{
			var checker= await   _userService.CreateUserAdmin(addUserVM.Username);
				if (checker)
				{
					TempData["success"] = "Successfully Saved";
					return View();
				}
			}
			catch(Exception ex)
			{

			  //  _loggerInfo.Error(ex.ToString(), "Asse::");
			}
			TempData["error"] = "User already existed";

			return View(addUserVM);
		}


		public JsonResult GetAllUsers(DataTableAjaxPostModel model)
		{
			// action inside a standard controller
		   // Guid obj = Guid.NewGuid();
		   // var g = obj;
			int filteredResultsCount;
			int totalResultsCount;
		   var result = _userService.GetAllUsersDataTable(model, out filteredResultsCount, out totalResultsCount);
			var data = result.Select(x => new {
				x.Id,
				Department =x.DepartmentName,
				Role = x.strUserRole,
				x.Username,
				// Url = "Account/Login"
			});
			return Json(new
			{
				model.draw,
				recordsTotal = totalResultsCount,
				recordsFiltered = filteredResultsCount,
				data
			}, JsonRequestBehavior.AllowGet);
		}
		public async Task<ActionResult> UserUpdate(int Id)
		{
			var user = await _userService.GetUserRecord(Id);	

			return View(user);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[LogActionFilter]
		public async Task<ActionResult> UserUpdate(AddUserVM addUserVM)
		{
			if (!ModelState.IsValid)
				return View(addUserVM);
			try
			{
				var checker = await _userService.UpdateUser(addUserVM, false);
				if (checker)
				{
					return new JsonResult { Data = new { status = checker } };
				}
			}
			catch (Exception ex)
			{

			}
			return new JsonResult { Data = new { status = false } };
		}

		public async Task<ActionResult> DeleteUser(int Id)
		{

			var user = await _userService.GetUserRecord(Id);

			return View(user);
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> DeleteUser(AddUserVM addUserVM)
		{
			if (!ModelState.IsValid)
				return View(addUserVM);
			try
			{
				var checker = await _userService.UpdateUser(addUserVM, true);
				if (checker)
				{
					return new JsonResult { Data = new { status = checker } };
				}
			}
			catch (Exception ex)
			{

			}
			return new JsonResult { Data = new { status = false } };
		}
		public ActionResult Logout()
		{
			try
			{
				Utility.RemoveFromCookie("UserRole");
				TempData.Clear();
				Session.Clear();
			}
			catch (Exception ex)
			{
				//_iLogger.Error(ex, string.Empty);
			}
			return RedirectToAction("login", "Account");
				
				//(ConfigurationManager.AppSettings["baseURL"]);
		}

	}
}