﻿using Newtonsoft.Json;
using NLog;
using OperationalRiskException.AuditLogFilter;
using OperationalRiskException.AuthorizationFilter;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.Helpers;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
	[CustomAuthorize(UserRoleEnum.OperationalRisk)]
	public class QuestionController : Controller
	{
		// GET: Question
		private readonly IQuestionCategoryService  _questionCategory;
		private readonly IQuestionService _question;
		private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();

		public QuestionController()
		{
			_questionCategory = new QuestionCategoryService();
			_question = new QuestionService();
		}


		//--Question Categore features--
		[LogActionFilter]
		public ActionResult QCategoryPage()
		{
			
			return View();
		}
		[LogActionFilter]
		public ActionResult QuestionPendingRequest()
		{
			return View();
		}
		[HttpPost]
		[LogActionFilter]
		public async Task<ActionResult> QCategoryPage(QuestionCategoryVM model)
		{
			if (ModelState.IsValid)
			{
				
			   var checker= await _questionCategory.AddQuestionCategory(model);
				if (checker)
				{
					TempData["CreateCat"] = "Question Category  created successfully";
					return View();
				}
				else
					TempData["Failed"] = "Question Category Name already existed";
			}


			// this did not work
			//model.CategoriesName = _questionCategory.GetQuestionCat()
			return View(model);
		}

		



		

		public JsonResult GetAllQuestionCategories(DataTableAjaxPostModel model)
		{
			int filteredResultsCount;
			int totalResultsCount;
			var result = _questionCategory.GetAllQuestionCategoriesDataTable(model, out filteredResultsCount, out totalResultsCount);
			return Json(new
			{
				model.draw,
				recordsTotal = totalResultsCount,
				recordsFiltered = filteredResultsCount,
				data=result
			}, JsonRequestBehavior.AllowGet);
		}

		public async Task<ActionResult> UpdateQCategory(int Id)
		{
			var getQc =await _questionCategory.GetSingleQuestionCategory(Id);
			return View(getQc);
		}

		[HttpPost]

		// this needs to be changed to UpdateQCategory from UpdateQCategoryAsync
		public async Task<ActionResult> UpdateQCategory(QuestionCategoryVM model)
		{
			bool status = false;
			if (ModelState.IsValid)
			{
			 status=  await _questionCategory.UpdateQuestionCategory(model);
			}
			return new JsonResult { Data = new { status } };
		 
		}

		public async Task<ActionResult> DeleteQCategory(int Id)
		{
			var getQc = await _questionCategory.GetSingleQuestionCategory(Id);
			return View(getQc);
		}
		[HttpPost]
		public ActionResult DeleteQCategory(QuestionCategoryVM categoryVM)
		{
			
			var getQc =  _questionCategory.DeleteQuestionCategory(categoryVM.CategoryId);
		   return new JsonResult { Data = new { status = getQc == 1} };
		}

		[LogActionFilter]
		public ActionResult QuestionPage()
		{
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();
			return View();
		}

		[HttpPost]
		public async Task<ActionResult> QuestionPage(QuestionVM model)
		{
			model.CreateUser = Username();
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();
			if (ModelState.IsValid)
			{
				if (model.QuestionType == QuestionTypeEnum.Options)
				{
					if (String.IsNullOrEmpty(model.AnswerTextA))
					{

						string errorMessage = "There is no Option text (A), Kindly fill the option text A ";
						TempData["FailedQuest"] = errorMessage;
						ModelState.AddModelError("AnswerTextA", errorMessage);
						return View(model);
						//TempData["FailedQuest"] = "There is no Option text (A), Kindly fill the option text A  ";
					

					}
					if (String.IsNullOrEmpty(model.AnswerTextB))
					{

						string errorMessage = "There is no Option text (B), Kindly fill the option text B";
						TempData["FailedQuest"] = errorMessage;
						ModelState.AddModelError("AnswerTextB", errorMessage);
						return View(model);

					}
					if (String.IsNullOrEmpty(model.AnswerTextC))
					{

						string errorMessage = "There is no Option text (C), Kindly fill the option text C ";
						TempData["FailedQuest"] = errorMessage;
						ModelState.AddModelError("AnswerTextC", errorMessage);
						return View(model);

					}
					if (String.IsNullOrEmpty(model.AnswerTextD))
					{

						string errorMessage = "There is no Option text (D), Kindly fill the option text D ";
						TempData["FailedQuest"] = errorMessage;
						ModelState.AddModelError("AnswerTextD", errorMessage);
						return View(model);

					}


				}

				var validation = await _question.GetValidation(model.QuestionText);
				if (!validation)
				{
					string errorMessage = "The question text already exist on the Portal";
					TempData["FailedQuest"] = errorMessage;
					return View(model);
				}
			
				var checkSave = await _question.AddQuestion(model);
				if (checkSave)
				{
					TempData["CreateQuest"] = "Question created successfully, It will be pending approval";
					//model.QuestionText = String.Empty;
					return View();
				}
				else
				{
					TempData["FailedQuest"] = "Error in adding question, Pls, Contact the admin";
					return View();
				}
				//TempData["CreateCat"] = "Question Category created successfully";
			}
			return View(model);
		}





		public JsonResult GetAllQuestions(DataTableAjaxPostModel model)
		{
			int filteredResultsCount;
			int totalResultsCount;
			var result = _question.GetAllQuestionDataTable(model, out filteredResultsCount, out totalResultsCount, Core.Entities.ApprovalStatus.Approved);
			return Json(new
			{
				model.draw,
				recordsTotal = totalResultsCount,
				recordsFiltered = filteredResultsCount,
				data = result
			}, JsonRequestBehavior.AllowGet);
		}
		public JsonResult Pending(DataTableAjaxPostModel model)
		{
			int filteredResultsCount;
			int totalResultsCount;

			var result = _question.GetAllQuestionDataTablePending(model, out filteredResultsCount, out totalResultsCount, Username(), Core.Entities.ApprovalStatus.Pending);
			return Json(new
			{
				model.draw,
				recordsTotal = totalResultsCount,
				recordsFiltered = filteredResultsCount,
				data = result
			}, JsonRequestBehavior.AllowGet);
		}
		public async Task<ActionResult> UpdateQuestion(int Id)
		{
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();

			var getQc = await _question.GetQuestion(Id);
			return View(getQc);
		}

		[HttpPost]
		public async Task<ActionResult> UpdateQuestion(QuestionVM model)
		{
			bool status = false;
			model.CreateUser = Username();
			if (ModelState.IsValid)
			{
				try
				{
					var updateStatus= await _question.UpdateQuestion(model);
					status = updateStatus;
					
				}
				catch (Exception ex) {
				}
			}
			return new JsonResult { Data = new { status = status } };

		}

		public async Task<ActionResult> UpdateQuestionNumeric(int Id)
		{
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();

			var getQc = await _question.GetQuestion(Id);
			return View(getQc);
		}

		[HttpPost]
		public async Task<ActionResult> UpdateQuestionNumeric(QuestionVM model)
		{
			bool status = false;
			model.CreateUser = Username();
			if (ModelState.IsValid)
			{
				try
				{
					var updateStatus = await _question.UpdateQuestion(model);
					status = updateStatus;

				}
				catch (Exception ex)
				{
				}
			}
			return new JsonResult { Data = new { status = status } };

		}
		public async Task<ActionResult> UpdateQuestionOption(int Id)
		{
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();

			var getQc = await _question.GetQuestion(Id);
			return View(getQc);
		}

		[HttpPost]
		public async Task<ActionResult> UpdateQuestionOption(QuestionVM model)
		{
			bool status = false;
			model.CreateUser = Username();
			if (ModelState.IsValid)
			{
				try
				{
					if (model.QuestionType == QuestionTypeEnum.Options)
					{
						if (String.IsNullOrEmpty(model.AnswerTextA))
						{

							string errorMessage = "There is no Option text (A), Kindly fill the option text A ";
							TempData["FailedQuest"] = errorMessage;
							ModelState.AddModelError("AnswerTextA", errorMessage);
							return View(model);
							//TempData["FailedQuest"] = "There is no Option text (A), Kindly fill the option text A  ";


						}
						if (String.IsNullOrEmpty(model.AnswerTextB))
						{

							string errorMessage = "There is no Option text (B), Kindly fill the option text B";
							TempData["FailedQuest"] = errorMessage;
							ModelState.AddModelError("AnswerTextB", errorMessage);
							return View(model);

						}
						if (String.IsNullOrEmpty(model.AnswerTextC))
						{

							string errorMessage = "There is no Option text (C), Kindly fill the option text C ";
							TempData["FailedQuest"] = errorMessage;
							ModelState.AddModelError("AnswerTextC", errorMessage);
							return View(model);

						}
						if (String.IsNullOrEmpty(model.AnswerTextD))
						{

							string errorMessage = "There is no Option text (D), Kindly fill the option text D ";
							TempData["FailedQuest"] = errorMessage;
							ModelState.AddModelError("AnswerTextD", errorMessage);
							return View(model);

						}


					}


					var updateStatus = await _question.UpdateQuestion(model);
					status = updateStatus;

				}
				catch (Exception ex)
				{
				}
			}
			return new JsonResult { Data = new { status = status } };

		}
		public async Task<ActionResult> DeleteQuestion(int Id)
		{
			var getQc = await _question.GetQuestion(Id);
			return View(getQc);
		}
		[HttpPost]
		public async Task<ActionResult> DeleteQuestion(QuestionVM question)
		{

			var getQc = await _question.DeleteQuestion(question.QuestionId);
			return new JsonResult { Data = new { status = getQc == 1 } };
		}
		[CustomAuthorize(UserRoleEnum.OperationalRisk)]
		public  ActionResult UploadQuestion()
		{
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();

			//var getQc = await _questionCategory.GetQuestion(Id);
			return View();
		}
		[HttpPost]

		public ActionResult UploadQuestion(QuestionVM question, HttpPostedFileBase FileUpload)
		{
			int TotalRows = 0;
			int SaveRows = 0;
			List<string> data = new List<string>();
			ViewBag.Categories = _questionCategory.GetAllQuestionCategories();
			if (FileUpload != null)
			{
				// tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
				if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
				{
					string filename = FileUpload.FileName;
					string targetpath = Server.MapPath("~/Doc/");
					if (!Directory.Exists(targetpath))
					{
						Directory.CreateDirectory(targetpath);
					}
					FileUpload.SaveAs(targetpath + filename);
					string pathToExcelFile = targetpath + filename;
					string filePath = Server.MapPath(filename);
					FileInfo file = new FileInfo(pathToExcelFile);
					byte[] bin = System.IO.File.ReadAllBytes(pathToExcelFile);


					//create a new Excel package in a memorystream
					using (MemoryStream stream = new MemoryStream(bin))
					{
						if (filename.EndsWith(".xls") || filename.EndsWith(".xlsx"))
						{
							try
							{
								 SaveRows = _question.ExcelUpload(question, stream, out TotalRows, Username());
								TempData["SuccessMsg"] = $"The TotalRows is {TotalRows-1}, Record Saves is {SaveRows} ";
								
								return View();
							}
							catch (Exception ex)
							{
								_loggerInfo.Error(ex.ToString());

							}


							//throw new Exception("File is not an excel sheet");
						}
					}
					//
					//deleting excel file from folder  
					if ((System.IO.File.Exists(pathToExcelFile)))
					{
						System.IO.File.Delete(pathToExcelFile);
					}
					ViewBag.Error = "";
					return View();
				}
				else
				{
					
				}
				return Json("error", JsonRequestBehavior.AllowGet);
			}
			else
			{
				return View();
			}
		}
		private string Username()
		{
			var user = new CookieController().ReadFromCookie("UserRole");
		   
			var deUser = JsonConvert.DeserializeObject<RoleViewModel>(user);
			return deUser.Username;
		}
		public  ActionResult Approve(int Id)
		{
			var model = new ApprovalVM
			{
				ApprovalStatus = Core.Entities.ApprovalStatus.Approved,
				QuestionId = Id,
				Username = Username()

			};
			return View(model);
		}

		[HttpPost]
		public async Task<ActionResult> Approve(ApprovalVM model)
		{
			try
			{
				await _question.QuestionApproval(model);
				return new JsonResult { Data = new { status = true } };
			}
			catch (Exception ex)
			{

			}
			return new JsonResult { Data = new { status = false } };


		}
		public ActionResult Reject(int Id)
		{
			var model = new ApprovalVM { ApprovalStatus = Core.Entities.ApprovalStatus.Rejected,
				 QuestionId=Id,
				 Username= Username()

			};
			return View(model);
		}

		[HttpPost]

		// changed the questionapprove to questionreject
		public async Task<ActionResult> Reject(ApprovalVM model)
		{
			try
			{
				await _question.QuestionReject(model);
				return new JsonResult { Data = new { status = true } };
			}catch(Exception ex)
			{

			}
			return new JsonResult { Data = new { status = false } };

		}

	}
}