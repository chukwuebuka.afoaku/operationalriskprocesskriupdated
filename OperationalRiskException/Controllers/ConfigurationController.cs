﻿using OperationalRiskException.AuditLogFilter;
using OperationalRiskException.AuthorizationFilter;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Services;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace OperationalRiskException.Controllers
{
    [CustomAuthorize(UserRoleEnum.OperationalRisk)]
    public class ConfigurationController : Controller
    {
        private IConfigurationService _configurationService;
        public ConfigurationController()
        {
            _configurationService = new ConfigurationService();
        }

        // GET: Configuration
        [LogActionFilter]
        public ActionResult Champion()
        {

           // var check = _configurationService.GetListOfDepartmentInNigeriaHO();
            return View();
        }

        public JsonResult Records(string country, bool checkHO )
        {
            var result = _configurationService.GetListOfDivision(country, checkHO);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RecordsByCountry(string country, bool checkHO)
        {
            var result = _configurationService.GetListOfDivision(country, checkHO);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AllUBACountriesExceptNG()
        {
            var result = _configurationService.GetAllUBACountriesExceptNG();
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllUserByDivision(string DivisionSOLID)
        {
            //string tesr=String.Format()
            var result = _configurationService.GetAllUBAStaffByDivision(DivisionSOLID);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllUserByDepartment(string DepartmentSOLID)
        {
            //string tesr=String.Format()
            var result = _configurationService.GetListOfUBA(DepartmentSOLID);
            return Json(new
            {
                data = result
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUserDetail(int id)
        {

            var check = _configurationService.GetUserDetails(id);
            return View("_AddUser", check);
        }
        public async Task<JsonResult> GetAddUserAsync(HR_STAFF_USER user)
        {
            var result =await _configurationService.AddChampion(user);
            var message = result ? "Successfully Added " + user.username : 
                "Failed (Check your records whether the user existed or network problem) " + user.username;
            return Json(new
            {
                data = result,
                message,
            }, JsonRequestBehavior.AllowGet);
        }





    }
}