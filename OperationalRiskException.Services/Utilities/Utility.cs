﻿using Newtonsoft.Json;
using NLog;
using OperationalRiskException.Core.Enums;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OperationalRiskException.Services.Utilities
{
    public static class Utility
    {

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();


        public static async Task<string> GetExternalAPI(string urlendpoint)
        {
            // var returnValue = new HRStaffObject();
            try
            {
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(urlendpoint);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync(urlendpoint); //.Result;
                    if (response != null && response.IsSuccessStatusCode && response.StatusCode == HttpStatusCode.OK)
                    {
                        // response?.EnsureSuccessStatusCode();
                        string response_string = await response.Content.ReadAsStringAsync(); //.Result;
                                                                                             //  _loggerInfo.Info(string.Format("Service Raw Response {0}", response_string));
                        return response_string;
                    }
                    else
                    {
                        return null;
                    }
                }

            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, string.Empty);
                return null;
            }
        }

      
            public static string GetBaseURL()
            {
                var url = ConfigurationManager.AppSettings["baseURL"];
                return url;
            }
      

        public static RoleViewModel GetRoleViewModel(int roleId, string Username = null)
        {
            try
            {
                RoleViewModel roleValue = new RoleViewModel
                {
                    RoleId = roleId,
                    Rolename = Enum.GetName(typeof(UserRoleEnum), roleId), 
                    Username = Username
                };
               
                return roleValue;
            }
            catch //(Exception ex)
            {
                return null;
            }
        }
        public static RoleViewModel GetRoleViewModel()
        {
            var userviewModel = HttpContext.Current.Request.Cookies["UserRole"];
            var userRole = userviewModel != null ? userviewModel.Value : string.Empty;
            var RoleView = string.IsNullOrWhiteSpace(userRole) ? new RoleViewModel() : JsonConvert.DeserializeObject<RoleViewModel>(userRole);
            return RoleView;
        }
        //public static string GetDescription<T>(this T e) where T : IConvertible
        //{
        //    if (e is Enum)
        //    {
        //        Type type = e.GetType();
        //        Array values = System.Enum.GetValues(type);

        //        foreach (int val in values)
        //        {
        //            if (val == e.ToInt32(CultureInfo.InvariantCulture))
        //            {
        //                var memInfo = type.GetMember(type.GetEnumName(val));
        //                var descriptionAttribute = memInfo[0]
        //                    .GetCustomAttributes(typeof(DescriptionAttribute), false)
        //                    .FirstOrDefault() as DescriptionAttribute;

        //                if (descriptionAttribute != null)
        //                {
        //                    return descriptionAttribute.Description;
        //                }
        //            }
        //        }
        //    }

        //    return null; // could also return string.Empty
        //}

        public static string GetDescription(this Enum enumValue)
        {
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            var descriptionAttributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : enumValue.ToString();
        }

        public static void RemoveFromCookie(string key)
        {
            
            System.Web.HttpCookie mycookie = new System.Web.HttpCookie(key)
            {
                Expires = DateTime.Now.AddHours(-1)
            };
            HttpContext.Current.Response.Cookies.Add(mycookie);

            System.Web.HttpCookie mycookie1 = new System.Web.HttpCookie("Username")
            {
                Expires = DateTime.Now.AddHours(-1)
            };
            HttpContext.Current.Response.Cookies.Add(mycookie1);

        }
        public static string GetUsername(string key="UserRole")
        {
            var aCookie = System.Web.HttpContext.Current.Request.Cookies[key];
            var userRole = aCookie != null ? aCookie.Value : string.Empty;
            if (!string.IsNullOrWhiteSpace(userRole))
            {
               var user= JsonConvert.DeserializeObject<RoleViewModel>(userRole);
                return user.Username;
            }
            return "Anonymous";
           
        }
        public static DateTime GetCurrentLocalDateTime()
        {
            // gives you current Time in server timeZone
            var serverTime = DateTime.Now;
            // convert it to Utc using timezone setting of server computer
            var utcTime = serverTime.ToUniversalTime();
            var tzi = TimeZoneInfo.FindSystemTimeZoneById("W. Central Africa Standard Time");
            // convert from utc to local
            var localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);
            return localTime;
        }
    }

}
