﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Interfaces
{
    public interface IEscalationService
    {
        Task ContinuousEscalationImplementation();
        Task FirstEscalationImplementation();
        Task SoftWarningEscalation();
    }
}
