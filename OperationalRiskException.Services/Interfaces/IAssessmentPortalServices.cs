﻿using OperationalRiskException.Core.Dtos;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Interfaces
{
    public interface IAssessmentPortalServices
    {
        Task<ChampionDashboardVM> GetAllRecords(string username);
        Task<List<QuestionMgtVM>> GetAllRecordAllQuestions(string username,int assSchId);
        Task<bool> SaveFile(FileUploadVM questionVM, string username, int SetUpId);
        Task<bool> SaveAssessment(SaveQuestionVM questionVM, string username, int SetUpId);
        Task<MainAnswerDto> GetAllSavedAssessment(string username, int QuestionCategoryId, int SetUpId);
        List<ScoreVM> GetAllScores();
        List<ScoreVM> GetAllScoreUser(string username);
        List<TestQuestionVM> GetCorrectiveFormByUsername(int schedulerId, string username);
        Task<bool> SaveCorrectivePlan(SaveQuestionVM questionVM, string username, int SetUpId);
        List<MainAnswerDto> GetAllSavedAssessments(int userId, int SetUpId);
        List<CorrectivePlanDisplayVM> getAllCorrectivePlan(int userId, int SetUpId);
        bool IsAssessmentValid(string username, int SchId);
    }
}
