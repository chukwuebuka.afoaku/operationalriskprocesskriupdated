﻿using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperationalRiskException.Services.Interfaces
{
    public interface IQuestionService
    {
        Task<bool> AddQuestion(QuestionVM model);
        Task<QuestionVM> GetQuestion(int Id);
        Task<int> DeleteQuestion(int questionId);
        IList<QuestionVM> GetAllQuestionDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount, ApprovalStatus approvalStatus);

        int ExcelUpload(QuestionVM question, Stream FileUpload, out int sheetCount, string username);
        Task<bool> UpdateQuestion(QuestionVM model);

        Task QuestionApproval(ApprovalVM model);
        Task QuestionReject(ApprovalVM model);
        IList<QuestionVM> GetAllQuestionDataTablePending(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount, string v, ApprovalStatus pending);
        Task<bool> GetValidation(string questionText);
    }
}
