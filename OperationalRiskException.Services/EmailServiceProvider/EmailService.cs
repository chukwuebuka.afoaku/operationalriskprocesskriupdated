﻿using NLog;
using OperationalRiskException.Core.Dtos;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OperationalRiskException.Services.EmailServiceProvider
{
  public  class EmailService : IEmailService
    {
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        public EmailService()
        {

        }

        public bool EscalationEmail(string supervUser, ChampionEscalationDTO pendingItem)
        {
            int countMails = 0;
           // string url = ConfigurationManager.AppSettings["templatePath"] as string;
            // string templatePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["templatePath"] as string);
            string url = ConfigurationManager.AppSettings["templatePath"] as string;
            string templatePath = HttpRuntime.AppDomainAppPath + url;
            var template = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["templatePath"] as string;

            string sender = ConfigurationManager.AppSettings["sender"] as string;
            string InCopy = ConfigurationManager.AppSettings["CC"] as string;
            InCopy +="," + pendingItem.Username;
            var cc = InCopy.Split(',');
          //  var cc = new List<string>();
            // string optRisk = ConfigurationManager.AppSettings["forensicGrp"] as string;
            // cc.Add(optRisk);

            
            string subject = "Escalation ";

            var userName = supervUser;
            if (!userName.Contains("@ubagroup.com"))
                userName = (userName + "@ubagroup.com").ToLower();
            string FirstName = userName.Split('.')[0].ToUpper();
            string body = GetBodyOfEscalation(pendingItem, template, FirstName);
            _loggerInfo.Info("Mail  Body::::", body);
            string response = SendEmail(userName, sender, subject, body, cc.ToArray());
            if (!(response == "Error"))
                countMails++;
            return countMails > 0;
        }
        public static string GetBodyOfEscalation(ChampionEscalationDTO model, string templatePath, string SupervisorName)
        {

            string body = "";
            try
            {
                using (var reader = new StreamReader(templatePath))
                {
                    body = reader.ReadToEnd();
                }
                string Username = model.Username.Replace("@ubagroup.com", "");
              
                body = body.Replace("{SupervisorName}", SupervisorName);
                body = body.Replace("{ChampionDetails}", Username.Split('.')[0].ToUpper()+ " "+ Username.Split('.')[1].ToUpper());

            }
            catch (Exception ex)
            {
                return "";
            }

            if (string.IsNullOrEmpty(body))
            {
                return "";
            }
            return body;

        }
        public string SendEmail(string recipient, string sender, string subject, string body, string[] cc)
        {
            string result = "";
            try
            {

                EmailServiceReference.Service objPayRef = new EmailServiceReference.Service();
                //cc
                result = objPayRef.SendMail(recipient, sender, subject, body, cc);

                _loggerInfo.Info("Sending Message" + result);
                return result;
            }
            catch (Exception exx)
            {
                _loggerInfo.Error("Stack:::" + exx.StackTrace + "InnerException:::" + exx.InnerException + "Message:::" + exx.Message,
                    "Assessment Error");
                return "Error";
            }

        }

        public bool SoftWarning(string supervUser, ChampionEscalationDTO pendingItem)
        {
            int countMails = 0;
            // string url = ConfigurationManager.AppSettings["templatePath"] as string;
            // string templatePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["templatePath"] as string);
            string url = ConfigurationManager.AppSettings["SoftWaringtemplatePath"] as string;
            string templatePath = HttpRuntime.AppDomainAppPath + url;
            string sender = ConfigurationManager.AppSettings["sender"] as string;
            string InCopy = ConfigurationManager.AppSettings["CC"] as string;
            var cc = InCopy.Split(',');
            //  var cc = new List<string>();
            // string optRisk = ConfigurationManager.AppSettings["forensicGrp"] as string;
            // cc.Add(optRisk);


            string subject = "Escalation Warning";

            var userName = pendingItem.Username;
            if (!userName.Contains("@ubagroup.com"))
                userName = (userName + "@ubagroup.com").ToLower();
            string FirstName = userName.Split('.')[0].ToUpper();
            string body = SoftWarningBodyEmail(pendingItem, templatePath, FirstName, supervUser);
            _loggerInfo.Info("Mail  Body::::", body);
            string response = SendEmail(userName, sender, subject, body, cc.ToArray());
            if (!(response == "Error"))
                countMails++;
            return countMails > 0;
        }
        public static string SoftWarningBodyEmail(ChampionEscalationDTO model, string templatePath, string ChampionName, string supervUser)
        {
            var daysConfigurationInterval = Convert.ToDouble(ConfigurationManager.AppSettings["StartDayInternal"].ToString());
            string body = "";
            try
            {
                using (var reader = new StreamReader(templatePath))
                {
                    body = reader.ReadToEnd();
                }
                string supervUserFullName = supervUser.Replace("@ubagroup.com", "");

                body = body.Replace("{ChampionName}", ChampionName);
                body = body.Replace("{EscalationStart}", model.EndDate.AddDays(daysConfigurationInterval).ToShortDateString());
                body = body.Replace("{SupervisorName}", supervUserFullName.Split('.')[0].ToUpper() + " " + supervUserFullName.Split('.')[1].ToUpper());

            }
            catch (Exception ex)
            {
                return "";
            }

            if (string.IsNullOrEmpty(body))
            {
                return "";
            }
            return body;

        }

    }
}
