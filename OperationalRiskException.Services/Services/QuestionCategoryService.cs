﻿using NLog;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OfficeOpenXml;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.Context;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Data.UnitOfWork;
using OperationalRiskException.Services.Interfaces;
using OperationalRiskException.Services.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OperationalRiskException.Services.Services
{
    public class QuestionCategoryService : IQuestionCategoryService
    {
        private IRepository<QuestionCategory> _questionCategoriesRepository;
        private IRepository<Question> _questionRepository;
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        private IUnitOfWork _unitOfWork;
    

        public QuestionCategoryService()
        {
            _questionCategoriesRepository = new Repository<QuestionCategory>();
            _questionRepository = new Repository<Question>();
            _unitOfWork = new UnitOfWork(new OperationalRiskContext());

    }
    public async Task<bool> AddQuestionCategory(QuestionCategoryVM model)
        {
            try
            {
                var checker = _questionCategoriesRepository.GetSingleRecordFilter(x => x.CategoryName.ToLower() == model.CategoriesName.ToLower() && !x.IsDeleted);
                if (checker != null)
                    throw new Exception("Question Category Name already existed");
                var entity = new QuestionCategory
                {
                    CategoryName = model.CategoriesName
                };
                _questionCategoriesRepository.Insert(entity);
              await  _questionCategoriesRepository.SaveAsync();
                return true;
            }
            catch(Exception ex)
            {
                
                _loggerInfo.Error(ex, "Error on Saving Question Category");
                return false;

            }

        }

        public int DeleteQuestionCategory(int Id)
        {
			try
			{
				var entity = _questionCategoriesRepository.GetSingleRecordFilter(x => x.Id == Id && !x.IsDeleted);
				if (entity != null)
				{
					_unitOfWork.BeginTransaction();
					var getAllQuestions = _questionRepository.GetAllIncluding(x => x.QuestionCategoryId == Id && !x.IsDeleted, true);
					foreach (var itemQuestion in getAllQuestions)
					{
						_questionRepository.Delete(itemQuestion);
						_questionRepository.Save();
					}
					_questionCategoriesRepository.Delete(entity);
					_unitOfWork.Commit();
					var deleteEn =  _questionCategoriesRepository.Save();
				
					return deleteEn;
				}
				else
				{
					return 0;
				}
			}
			catch (Exception ex)
			{
				_unitOfWork.RollBack();
                _loggerInfo.Error(ex, "Error on Saving Question Category");
                return 0;
               // throw new Exception("Invalid records");
               
            }

        }
        public async Task<QuestionCategoryVM> GetSingleQuestionCategory(int Id)
        {
			var entity = await _questionCategoriesRepository.GetByIdAsync(Id);
			var qc = new QuestionCategoryVM
			{
				CategoriesName = entity.CategoryName,
				CategoryId = entity.Id
			};
			return qc;

		}

		public async Task<bool> UpdateQuestionCategory(QuestionCategoryVM model)
        {
            try
            {
                var entity =await _questionCategoriesRepository.GetSingleRecordFilterAsync(x => x.Id == model.CategoryId && !x.IsDeleted );


                if (entity == null)
                    throw new Exception("Record does not exist");
                var checkRecord = _questionCategoriesRepository.GetSingleRecordFilter(x => x.CategoryName.ToLower() == model.CategoriesName.ToLower() && !x.IsDeleted);
                if (checkRecord != null)
                    throw new Exception("Record already existed");

                entity.CategoryName = model.CategoriesName;
                _questionCategoriesRepository.Update(entity);
                var updateEn =  _questionCategoriesRepository.Save();
                if(updateEn==0)
                    throw new Exception("Failed to update");
                return true;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Error on Saving Question Category");
                return false;

            }
}

        public IEnumerable<QuestionCategoryVM> GetQuestionCat()
        {
            var QCategories = _questionCategoriesRepository.GetAllRecordsFilters(x => x.IsDeleted == false);
            var records= QCategories.Select(x => new QuestionCategoryVM
            {
                CategoriesName = x.CategoryName,
                CategoryId=x.Id
            }) ;
            return records;
        }


        public IList<QuestionCategoryVM> GetAllQuestionCategoriesDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            // search the dbase taking into consideration table sorting and paging
            var result = GetDataFromDbase(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount);
            if (result == null)
            {
                result = new List<QuestionCategoryVM>();
                return result;
            }
            return result;
        }
        private List<QuestionCategoryVM> GetDataFromDbase(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount)
        {
            //var role = Enum.Parse(typeof(UserRoleEnum), searchBy);
            var record = _questionCategoriesRepository.GetAllIncluding(x => !x.IsDeleted, false);
            totalResultsCount = record.Count();
            if (!String.IsNullOrEmpty(searchBy))
                record = record.Where(x => x.CategoryName.ToLower().Contains(searchBy.ToLower()));
            //SortBy
            record = sortBy == "CategoriesName" ? record.OrderBy<QuestionCategory>(x => x.CategoryName, sortDir):
                  record.OrderByDescending(x => x.Id);
            filteredResultsCount = record.Count();
            var result = record.Skip(skip).Take(take).ToList();
            return result.Select(x => new QuestionCategoryVM { CategoriesName=x.CategoryName,
                CategoryId=x.Id
            }).ToList();
        }


        public IEnumerable<QuestionCategoryVM> GetAllQuestionCategories()
        {
            var record = _questionCategoriesRepository.GetAllIncluding(x => !x.IsDeleted, false).ToList();

            return record.Select(x => new QuestionCategoryVM
            {
                CategoriesName = x.CategoryName,
                CategoryId = x.Id
            }).ToList();

        }

    }

}
