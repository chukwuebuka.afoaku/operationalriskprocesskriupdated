﻿using NLog;
using OperationalRiskException.Core.Dtos;
using OperationalRiskException.Core.Entities;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace OperationalRiskException.Services.Services
{

    public class AssessmentNotificationService  : IAssessmentNotificationService
    {
        private IRepository<AssessmentScheduling> _assessmentScheduler;
        private IRepository<AssessmentSetup> _assessmentSetup;
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        public AssessmentNotificationService() {
            _assessmentScheduler = new Repository<AssessmentScheduling>();
            _assessmentSetup = new Repository<AssessmentSetup>();
        }
        private List<UserWithAssessmentDate> DailyNotifications(DateTime dateTime)
        {
            var users = new List<UserWithAssessmentDate>();
            try
            {
                var presentDate = dateTime.Date.ToString("yyyy-MM-dd");
                var assessmentList = _assessmentSetup.GetWithRawSql($"select * from AssessmentSetups WHERE '{presentDate}' between StartDate AND EndDate").Select(x => x.Id);
                users = _assessmentScheduler.GetAllIncluding(x => !x.IsDeleted && !x.IsAnswered, false, x => x.User, x => x.AssessmentSetup)
                   .Where(x => assessmentList.Contains(x.AssessmentSetupId)).Select(y => new UserWithAssessmentDate
                   {
                       Username = y.User.Username,
                       ReliefEmail = y.User.ReliefEmail,
                       StartDate = y.AssessmentSetup.StartDate,
                       EndDate = y.AssessmentSetup.EndDate,
                       DepartmentName = y.User.DepartmentName,
                   }).ToList();
                _loggerInfo.Info("Count Users"+users.Count);
            }catch(Exception exx)
            {
                _loggerInfo.Error("Stack:::" + exx.StackTrace + "InnerException:::" + exx.InnerException +"Message:::" + exx.Message,
                    "Assessment Error");
            }
            return users;
        }
        private List<UserWithAssessmentDate> NotificationBeforeAssessmentPeriod(DateTime dateTime)
        {
            var users = new List<UserWithAssessmentDate>();
            try
            {
                var presentDate = dateTime.Date.ToString("yyyy-MM-dd");
                var assessmentList = _assessmentSetup.GetWithRawSql($"select * from AssessmentSetups WHERE '{presentDate}' < StartDate").Select(x => x.Id);
                users = _assessmentScheduler.GetAllIncluding(x => !x.IsDeleted && !x.IsAnswered, false, x => x.User, x => x.AssessmentSetup)
                   .Where(x => assessmentList.Contains(x.AssessmentSetupId)).Select(y => new UserWithAssessmentDate
                   {
                       Username = y.User.Username,
                       ReliefEmail = y.User.ReliefEmail,
                       StartDate = y.AssessmentSetup.StartDate,
                       EndDate = y.AssessmentSetup.EndDate,
                       DepartmentName = y.User.DepartmentName,
                   }).ToList();
                _loggerInfo.Info("Count Users" + users.Count);
            }
            catch (Exception exx)
            {
                _loggerInfo.Error("Stack:::" + exx.StackTrace + "InnerException:::" + exx.InnerException + "Message:::" + exx.Message,
                    "Assessment Error");
            }
            return users;
        }
        public int EmailSendingPeriodOfAssessment(string _templatePath)
        {
            int countMails = 0;
            var users = DailyNotifications(DateTime.Now);
            string templatePath = _templatePath;
            string sender = ConfigurationManager.AppSettings["sender"] as string;
          //  var cc = new List<string>();
            string optRisk = ConfigurationManager.AppSettings["CC"] as string;
            var CC = optRisk.Split(',');
            string subject = "Assessment Notification ";
            users.Add(new UserWithAssessmentDate { 
            ReliefEmail= "chukwuebuka.afoaku@ubagroup.com",
            Username= "chukwuebuka.afoaku@ubagroup.com",
            DepartmentName="CIO",
            EndDate=DateTime.Now,
            StartDate=DateTime.Now,
            
            });
            foreach(var usr in users )
            {
               // cc.Add(usr.ReliefEmail);
                string FirstName = usr.Username.Split('.')[0].ToUpper();
                string body = GetMailBody(FirstName, usr.StartDate.ToLongDateString(), usr.EndDate.ToLongDateString(), templatePath);
               string response=  SendEmail(usr.Username, sender, subject, body, CC.ToArray());
                if(!(response=="Error"))
                countMails++;
            }

           
            return countMails;
        }
     
        public string SendEmail(string recipient, string sender, string subject, string body, string[] cc)
        {
            string result = "";
            try
            {

                EmailServiceReference.Service objPayRef = new EmailServiceReference.Service();
                //cc
             result = objPayRef.SendMail(recipient, sender, subject, body, cc);

                _loggerInfo.Info("SMS Message" + result);
            return result;
        }
            catch (Exception exx)
            {
                _loggerInfo.Error("Stack:::" + exx.StackTrace + "InnerException:::" + exx.InnerException + "Message:::" + exx.Message,
                    "Assessment Error");
                return "Error";
            }
           
        }
        public static string GetMailBody(string Champion, string StartDate, string EndDate, string templatePath)
        {
            string body="";
            try
            {
                using (var reader = new StreamReader(templatePath))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Champion}", Champion);
                body = body.Replace("{StartDate}", StartDate);
                body = body.Replace("{EndDate}", EndDate);

            }
            catch (Exception ex)
            {
                return "";
            }

            if (string.IsNullOrEmpty(body))
            {
                return "";
            }
            return body;

        }



        public static string GetMailExceptionBody(string Champion, string templatePath2)
        {
            string body = "";
            try
            {
                using(var reader = new StreamReader(templatePath2))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Champion}", Champion);

            }
            catch(Exception ex)
            {
                return "";
            }

            if (string.IsNullOrEmpty(body))
            {
                return "";
            }

            return body;
        }



    }
}
