﻿using OperationalRiskException.Core.Entities;
using OperationalRiskException.Core.AuthVM;
using OperationalRiskException.Core.ViewModels;
using OperationalRiskException.Data.GenericRepository.IRepository;
using OperationalRiskException.Data.GenericRepository.Repository;
using OperationalRiskException.Services.AuthenticationServices;
using OperationalRiskException.Services.Interfaces;
using System.Threading.Tasks;
using NLog;
using OperationalRiskException.Core.Enums;
using System.Collections.Generic;
using System;
using System.Linq;
using OperationalRiskException.Core.DataTableModels;
using OperationalRiskException.Services.Utilities;
using System.Configuration;
using Newtonsoft.Json;

namespace OperationalRiskException.Services.Services
{
    public class UserService : IUserService
    {
        private IRepository<User> _userRepository;
        private IRepository<Department> _deptRepository;
        private static readonly Logger _loggerInfo = LogManager.GetCurrentClassLogger();
        public UserService()
        {
            _userRepository = new Repository<User>();
            _deptRepository = new Repository<Department>();
        }

        public Select2PagedResult GetSelect2PagedResult(string searchTerm, int pageSize, int pageNumber)
        {
           return AuthService.GetSelect2PagedResult( searchTerm,  pageSize,  pageNumber);
        }

        public User CheckUserExistence(string username)
        {
            var user =  _userRepository.GetSingleRecordFilter(x => x.Username == username && !x.IsDeleted);
            return user;
        }

        public AuthenticationResponce TokenAuthenticateAsync(EntrustRequest _param)
        {
            _loggerInfo.Info("EntrustRequest::: " + JsonConvert.SerializeObject(_param));
            return  AuthService.TokenAuthenticateAsync(_param);
        }

        public async Task<bool> UserExisted(int Id)
        {
            var check =await _userRepository.GetByIdAsync(Id);
            return check != null;
        }

        public async Task<bool> ValidateUserOnADAsync(LoginViewModel loginModel)
        {
            return await AuthService.ValidateUserOnADAsync(loginModel);
        }

        public async Task<bool> ValidateUserOnADAsyncMock(LoginViewModel loginModel)
        {
            return await AuthService.ValidateUserOnADAsyncMock(loginModel);
        }

        public IEnumerable<UserRoleEnum> GetUserROles()
        {
         return   Enum.GetValues(typeof(UserRoleEnum)).Cast<UserRoleEnum>(); ;
        }



        public IList<User> GetAllUsersDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount)
        {
            var searchBy = (model.search != null) ? model.search.value : null;
            var take = model.length;
            var skip = model.start;
            string sortBy = "";
            bool sortDir = true;

            if (model.order != null)
            {
                // in this example we just default sort on the 1st column
                sortBy = model.columns[model.order[0].column].data;
                sortDir = model.order[0].dir.ToLower() == "asc";
            }
            // search the dbase taking into consideration table sorting and paging
            var result = GetDataFromDbase(searchBy, take, skip, sortBy, sortDir, out filteredResultsCount, out totalResultsCount);
            if (result == null)
            {
                result = new List<User>();
                return result;
            }
            return result;
        }
        private List<User> GetDataFromDbase(string searchBy, int take, int skip, string sortBy, bool sortDir, out int filteredResultsCount, out int totalResultsCount)
        {
            //var role = Enum.Parse(typeof(UserRoleEnum), searchBy);
        var record = _userRepository.GetAllIncluding(x => !x.IsDeleted, false);
                totalResultsCount = record.Count();
            if (!String.IsNullOrEmpty(searchBy))
                record = record.Where(x => x.Username.ToLower().Contains(searchBy.ToLower())
                || x.DepartmentName.ToLower().Contains(searchBy.ToLower())
                  || x.strUserRole.ToLower().Contains(searchBy.ToLower())
                );
            //SortBy
                record =sortBy == "Username"? record.OrderBy<User>(x => x.Username, sortDir)
                    : sortBy == "Department"? record.OrderBy<User>(x => x.DepartmentName, sortDir):
                     sortBy == "Role" ? record.OrderBy<User>(x => x.strUserRole, sortDir):
                      record.OrderByDescending(x=>x.Id);
            filteredResultsCount = record.Count();
            var result = record.Skip(skip).Take(take).ToList();
                return result;
        }
        public async Task<HRStaffObject> GetHRStaff(string username)
        {
            if (username.Contains("@ubagroup.com"))
                username = username.Replace("@ubagroup.com", "");
            var urlendpoint = ConfigurationManager.AppSettings["SearchUseronAD"].Replace("{samName}", username);
            var response = await Utility.GetExternalAPI(urlendpoint);
            var hR = new HRStaffObject();
            if (response != null)
            {
                 hR = JsonConvert.DeserializeObject<HRStaffObject>(response);
            }
            return hR;
        }
        public async Task<bool> CreateUserAdmin(string username)
        {
            try
            {
                var hruser = await GetHRStaff(username);
                var user = new AddUserVM();
                if (hruser == null)
                    throw new Exception("User doesn't exist");
                user.DepartmentName = hruser.Department;
                user.Username = hruser.emailAddress;
                user.UserRole = UserRoleEnum.OperationalRisk;
              var x= await  CreateUser(user);
                return x;







            }
            catch (Exception ex) {
                
                    _loggerInfo.Info(ex, "Create OperationalRiskUser");
                return false;
            }
        }


            public async Task<bool> CreateUser(AddUserVM userVM)
        {
            int deptId = 0;
            try {
                var records= CheckUserExistence(userVM.Username);
                if (records != null)
                    throw new Exception("User already existed");
                var user = new User
                {
                    Username = userVM.Username,
                    DepartmentName=userVM.DepartmentName,
                    DepartmentId=userVM.DepartmentId,
                    UserRole=userVM.UserRole,
                    strUserRole=Enum.GetName(typeof(UserRoleEnum),userVM.UserRole),
                    CreatedBy=userVM.CreatedBy

                };
                var checkDepartment =_deptRepository.GetSingleRecordFilter(x => x.DepartmentName == user.DepartmentName);
                if (checkDepartment == null)
                {
                    var dept = new Department
                    {
                        CreatedBy = user.CreatedBy,
                        DepartmentName = user.DepartmentName
                    };
                    _deptRepository.Insert(dept);
                    _deptRepository.Save();
                    deptId = dept.Id;
                    _loggerInfo.Info( "Create Department::::  DeptId"+deptId);
                }
                else
                    deptId = checkDepartment.Id;
                //check user;
                var usercheck = _userRepository.GetSingleRecordFilter(x => x.Username == user.Username && !x.IsDeleted);
                if (usercheck == null)
                {
                    user.DepartmentId = deptId;
                    _userRepository.Insert(user);
                    var check = await _userRepository.SaveAsync();
                    return check == 1;
                }
                return false;
            } catch(Exception ex) {
                _loggerInfo.Error(ex, "Create User:::: ");
                return false;
            }
         

        }

		public async Task<AddUserVM> GetUserRecord(int Id) {
			var user = await _userRepository.GetByIdAsync(Id);
			var userVm = new AddUserVM {
				Id=user.Id,
				UserRole=user.UserRole,
				Username=user.Username,
                
			};
			return userVm;
		}

		public async Task<bool> UpdateUser(AddUserVM updateUserVM, bool IsDeleted)
        {
            try
            {
                var user = _userRepository.GetSingleRecordFilter(x => x.Id == updateUserVM.Id);
                if (IsDeleted)
                    _userRepository.Delete(user);
                else
                {
                    user.UserRole = updateUserVM.UserRole;
                    user.strUserRole = Enum.GetName(typeof(UserRoleEnum), user.UserRole);
                    _userRepository.Update(user);
                }
                var check = await _userRepository.SaveAsync();
                return check == 1;
            }
            catch (Exception ex)
            {
                _loggerInfo.Error(ex, "Update User/Deleted Method:::: ");
                return false;
            }
        }
    }
   
}
